package Main;

import Test.SoftwareTest;
import GUI.SoftwareFrame;

public class Main {
	
	public static void main(String[] args) {
		
		SoftwareTest control = new SoftwareTest();
		GUI.SoftwareFrame view = new GUI.SoftwareFrame(control);
		view.softwareframe();
	}

}

