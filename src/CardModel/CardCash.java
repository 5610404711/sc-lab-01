package CardModel;

public class CardCash {
	
	private double balance;
	
	public CardCash(){
		
		balance = 0;
	}
	
	public void addmoney(double amount){
		this.balance = balance + amount;
	}
	
	public void removemoney(double price){
		this.balance = balance - price;
	}
	
	public double getBalance(){
		return balance;
	}

}
