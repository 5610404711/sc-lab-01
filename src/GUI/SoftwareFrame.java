package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import Test.SoftwareTest;

public class SoftwareFrame implements ActionListener {
	private JFrame frame;
	private JLabel wordtopup;
	private JLabel wordamounttopup;
	private JButton buttontopup;
	private JLabel wordshop;
	private JButton buttonshop;
	private JTextField inputamount;
	private JLabel wordpriceshop;
	private JTextField inputprice;
	private JLabel wordresult;
	private JLabel outputresult;
	private JPanel panelTopUp1;
	private JPanel panelTopUp2;
	private JPanel panelShopMachine1;
	private JPanel panelShopMachine2;
	private JPanel panelmain;
	private JPanel panelresult;
	
	private SoftwareTest control;
	private JLabel wordbath;
	
	public SoftwareFrame(SoftwareTest control){
		this.control = control;
	}
	
	public void softwareframe(){
		frame = new JFrame("<<<<<CardCash>>>>>");
		
		panelTopUp1 = new JPanel();
		panelTopUp2 = new JPanel();
		panelShopMachine1 = new JPanel();
		panelShopMachine2 = new JPanel();
		panelmain = new JPanel();
		panelresult = new JPanel();
		
		wordtopup = new JLabel("�������Թ");
		wordtopup.setForeground(new Color(255,102,0));
		wordamounttopup = new JLabel("\n     �ӹǹ�Թ:  ");
		inputamount = new JTextField();
		buttontopup = new JButton("����Թ");
		buttontopup.addActionListener(this);
		
		wordshop = new JLabel("��ҹ���");
		wordshop.setForeground(new Color(0,153,0));
		wordpriceshop = new JLabel("\n     �Ҥ�:  ");
		inputprice = new JTextField();
		buttonshop = new JButton("���ͧ͢");
		buttonshop.addActionListener(this);
		
		wordresult = new JLabel("�Թ�������:  ");
		outputresult = new JLabel(""+control.getBalance());
		wordbath = new JLabel("�ҷ");
		wordresult.setHorizontalAlignment(SwingConstants.CENTER);
		outputresult.setHorizontalAlignment(SwingConstants.CENTER);
		
		panelTopUp2.setLayout(new GridLayout(1,2));
		panelTopUp2.add(wordamounttopup);
		panelTopUp2.add(inputamount);
		
		panelTopUp1.add(wordtopup);
		panelTopUp1.add(panelTopUp2);
		panelTopUp1.add(buttontopup);
		
		panelShopMachine2.setLayout(new GridLayout(1,2));
		panelShopMachine2.add(wordpriceshop);
		panelShopMachine2.add(inputprice);
		
		panelShopMachine1.add(wordshop);
		panelShopMachine1.add(panelShopMachine2);
		panelShopMachine1.add(buttonshop);
		
		panelmain.setLayout(new GridLayout(1,2));
		panelmain.add(panelTopUp1);
		panelmain.add(panelShopMachine1);
		
		
		panelresult.setLayout(new GridLayout(1,3));
		panelresult.add(wordresult);
		panelresult.add(outputresult);
		panelresult.add(wordbath);
		
		panelTopUp2.setVisible(true);
		panelTopUp1.setVisible(true);
		panelShopMachine2.setVisible(true);
		panelShopMachine1.setVisible(true);
		panelmain.setVisible(true);
		panelresult.setVisible(true);
		
		frame.setSize(700, 300);
		frame.add(panelmain,BorderLayout.CENTER);
		frame.add(panelresult,BorderLayout.SOUTH);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public void actionPerformed(ActionEvent event){
		
		String your_button = event.getActionCommand();
		
		if(event.getSource()instanceof JButton){
			if("����Թ"==your_button){
				Double input_amount = Double.parseDouble(inputamount.getText());
				control.setTopup(input_amount);
				outputresult.setText(""+control.getBalance());
				
			}else if("���ͧ͢"==your_button){
				Double input_price = Double.parseDouble(inputprice.getText());
				control.setShopmachine(input_price);
				outputresult.setText(""+control.getBalance());
			}
		}
	}
	
}
