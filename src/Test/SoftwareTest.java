package Test;

import CardModel.CardCash;
import CardModel.TopupMachine;
import CardModel.ShopMachine;


public class SoftwareTest {
	
	CardCash card = new CardCash();
	TopupMachine topup = new TopupMachine();
	CardModel.ShopMachine shopmachine = new CardModel.ShopMachine();
	
	public void setTopup(double amount){
		topup.refillmoney(card,amount);
	}
	
	public void setShopmachine(double amount){
		shopmachine.enterpayment(card,amount);
	}
	
	public double getBalance(){
		return card.getBalance();
	}
	

}
